﻿namespace ClientGestionVin.Models
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;

    public partial class Category
    {
        /// <summary>
        /// Initializes a new instance of the Product class.
        /// </summary>
        public Category() { }

        /// <summary>
        /// Initializes a new instance of the Product class.
        /// </summary>
        public Category(int? id = default(int?), string designation = default(string))
        {
            Id = id;
            Designation = designation;
        }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "designation")]
        public string Designation { get; set; }

        public int Quantite { get; set; }
    }
}
