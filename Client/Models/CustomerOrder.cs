﻿using ClientGestionVin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Client.Models
{
    class CustomerOrder
    {
        public CustomerOrder() { }

        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        [JsonProperty(PropertyName = "date")]
        public DateTime Date{ get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public int Quantite { get; set; }

        [JsonProperty(PropertyName = "product")]
        public Product Product { get; set; }
        [JsonProperty(PropertyName = "productId")]
        public int ProductId { get; set; }

        [JsonProperty(PropertyName = "customer")]
        public Product Customer { get; set; }
        [JsonProperty(PropertyName = "customerId")]
        public int CustomerId { get; set; }

        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Societe{ get; set; }
        public string DesignationProduit { get; set; }
        public double TU { get; set; }
        public double Total { get; set; }
        public bool IsRegister{ get; set; }
        public string Visibility { get; set; }
        public string Color { get; set; }
        public bool StockEnought { get; internal set; }
    }
}