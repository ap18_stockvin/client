﻿namespace ClientGestionVin.Models
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;

    public partial class Product
    {
        public Product() { }

        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        [JsonProperty(PropertyName = "designation")]
        public string Designation { get; set; }

        [JsonProperty(PropertyName = "annee")]
        public int? Annee { get; set; }

        [JsonProperty(PropertyName = "prix")]
        public double Prix { get; set; }

        [JsonProperty(PropertyName = "domaine")]
        public Domain Domaine { get; set; }

        [JsonProperty(PropertyName = "domainId")]
        public int DomainId { get; set; }

        [JsonProperty(PropertyName = "seuil")]
        public int Seuil { get; set; }

        [JsonProperty(PropertyName = "quantite")]
        public int Quantite { get; set; }

        [JsonProperty(PropertyName = "categorie")]
        public Category Categorie { get; set; }

        [JsonProperty(PropertyName = "categorieId")]
        public int CategorieId { get; set; }
        
        public string Famille { get; set; }
        public string Fournisseur { get; set; }

        public bool IsModified { get; set; }
    }
}