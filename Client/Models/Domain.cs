﻿namespace ClientGestionVin.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public partial class Domain
    {
        /// <summary>
        /// Initializes a new instance of the Product class.
        /// </summary>
        public Domain() { }

        /// <summary>
        /// Initializes a new instance of the Product class.
        /// </summary>
        public Domain(int? id = default(int?), string designation = default(string))
        {
            Id = id;
            Designation = designation;
        }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "designation")]
        public string Designation { get; set; }

        public int Quantite { get; set; }

    }
}
