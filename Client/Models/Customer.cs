﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Client.Models
{
    class Customer
    {
        public Customer() { }

        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        [JsonProperty(PropertyName = "prenom")]
        public string Prenom { get; set; }

        [JsonProperty(PropertyName = "nom")]
        public string Nom { get; set; }

        [JsonProperty(PropertyName = "societe")]
        public string Societe { get; set; }

        public int? orderLinked { get; set; }
    }
}