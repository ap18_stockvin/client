﻿using ClientGestionVin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Client.Models
{
    class DomainOrder
    {
        public DomainOrder() { }

        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        [JsonProperty(PropertyName = "date")]
        public DateTime Date{ get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public int Quantite { get; set; }

        [JsonProperty(PropertyName = "product")]
        public Product Product { get; set; }
        [JsonProperty(PropertyName = "productId")]
        public int ProductId { get; set; }

        [JsonProperty(PropertyName = "domain")]
        public Product Domain { get; set; }

        [JsonProperty(PropertyName = "domainId")]
        public int DomainId { get; set; }

        public string Fournisseur{ get; set; }
        public string DesignationProduit { get; set; }
        public double TU { get; set; }
        public double Total { get; set; }
        public bool IsRegister{ get; set; }
        public string Visibility { get; set; }
    }
}