﻿using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using ClientGestionVin.Pages;
using ClientGestionVin.Pages.Category;
using ClientGestionVin.Pages.Product;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
namespace ClientGestionVin.Pages
{
    public sealed partial class DashBord : Page
    {
        public DashBord()
        {
            this.InitializeComponent();
        }
        private void NavView_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (NavigationViewItemBase item in NavView.MenuItems)
            {
                if (item is NavigationViewItem && item.Tag.ToString() == "home")
                {
                    NavView.SelectedItem = item;
                    break;
                }
            }
        }
        private void NavView_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            if (args.IsSettingsInvoked)
            {
                ContentFrame.Navigate(typeof(ListProduct));
            }
            else
            {
                // find NavigationViewItem with Content that equals InvokedItem
                var item = sender.MenuItems.OfType<NavigationViewItem>().First(x => (string)x.Content == (string)args.InvokedItem);
                NavView_Navigate(item as NavigationViewItem);
            }
        }
        private void NavView_Navigate(NavigationViewItem item)
        {
            switch (item.Tag)
            {
                case "rouge":
                    ContentFrame.Navigate(typeof(ListProduct),"1");
                    break;

                case "blanc":
                    ContentFrame.Navigate(typeof(ListProduct),"2");
                    break;

                case "rose":
                    ContentFrame.Navigate(typeof(ListProduct),"3");
                    break;

                case "digestif":
                    ContentFrame.Navigate(typeof(ListProduct),"4");
                    break;

                case "petillant":
                    ContentFrame.Navigate(typeof(ListProduct),"5");
                    break;

                case "aperitif":
                    ContentFrame.Navigate(typeof(ListProduct),"6");
                    break;

                case "all":
                    ContentFrame.Navigate(typeof(ListProduct),"90");
                    break;
            }
        }       
        private void Button_all(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(ListProduct),"90");
        }
        private void Button_rouge(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(ListProduct), "1");
        }
        private void Button_blanc(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(ListProduct), "2");
        }
        private void Button_rose(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(ListProduct), "3");
        }
        private void Button_digestif(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(ListProduct), "4");
        }
        private void Button_petillant(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(ListProduct), "5");
        }
    }
}
