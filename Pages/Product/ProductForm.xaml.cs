﻿using ClientGestionVin.Helpers;
using ClientGestionVin.Client.Models;
using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ClientGestionVin.Models;
using System.Threading.Tasks;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace ClientGestionVin.Pages.Product
{
    public sealed partial class ProductForm : Page
    {
        private DomainRepository repoDomain;
        private ProductRepository productRepository;
        private DomainRepository domainRepository;
        private CategoryRepository categoryRepository;

        public List<Models.Category> Categories { get; private set; }
        public List<Models.Domain> Domains { get; private set; }

        private Dictionary<int, string> domainsDictionary;
        private Dictionary<int, string> categoriesDictionary;

        public List<Models.Product> Products { get; private set; }

        public int Editing { get; private set; }

        public ProductForm()
        {
            repoDomain = new DomainRepository();
            productRepository = new ProductRepository();
            domainRepository = new DomainRepository();
            categoryRepository = new CategoryRepository();

            fetchAsyncObjects();
            fetchCategoriesAsync();

            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            if (null != parameters)
            {
                int ProductId = int.Parse(string.Format("{0}", parameters));
                Editing = ProductId;
                if (0 != Editing)
                {
                    fetchProduct();
                }
            }
        }


        private async void fetchAsyncObjects()
        {
            await fetchDomainsAsync();
        }


        private async void fetchCategoriesAsync()
        {
            Categories = await categoryRepository.GetCategories();
            categoriesDictionary = new Dictionary<int, string>();

            foreach (var item in Categories)
            {
                categoriesDictionary[(int)item.Id] = item.Designation;
            }

            ProductFamilly.ItemsSource = categoriesDictionary;
        }

        private async Task fetchDomainsAsync()
        {
            Domains = await domainRepository.GetDomains();
            domainsDictionary = new Dictionary<int, string>();

            foreach (var item in Domains)
            {
                domainsDictionary[(int)item.Id] = item.Designation;
            }

            ProductFournisseur.ItemsSource = domainsDictionary;
        }

        private async void fetchProduct()
        {
            Models.Product product = await productRepository.GetProduct(Editing);
            ProductDesignation.Text = product.Designation;
            ProductPrice.Text = product.Prix.ToString();
            ProductYear.Text = product.Annee.ToString();
            ProductQuantity.Text = product.Quantite.ToString();
            ProductFournisseur.SelectedValuePath = domainsDictionary[product.DomainId];
            ProductFamilly.SelectedValuePath = categoriesDictionary[product.CategorieId];
            ProductTreshold.Text = product.Seuil.ToString();
        }

        private async void SaveProductAsync(object sender, object args)
        {
            KeyValuePair<int, string> category = (KeyValuePair<int, string>)ProductFamilly.SelectedItem;
            KeyValuePair<int, string> domain = (KeyValuePair<int, string>)ProductFournisseur.SelectedItem;

            int? year = null;
            if ("" != ProductYear.Text) {
                year = int.Parse(ProductYear.Text); 
            }

            Models.Product produit = new Models.Product
            {
                Designation = ProductDesignation.Text,
                Prix = double.Parse(ProductPrice.Text),
                Annee = year,
                Quantite = int.Parse(ProductQuantity.Text),
                DomainId = domain.Key,
                CategorieId = category.Key,
                Seuil = int.Parse(ProductTreshold.Text)
            };

            if (0 != Editing)
            {
                produit.Id = Editing;
                _ = await productRepository.UpdateProduct(produit);
            }
            else
            {
                _ = await productRepository.CreateProduct(produit);
            }

            var grid = VisualTreeHelper.GetParent(Frame);
            var page = VisualTreeHelper.GetParent(grid);
            var contentPresenter = VisualTreeHelper.GetParent(page);
            var frame = VisualTreeHelper.GetParent(contentPresenter) as Frame;

            frame.Navigate(typeof(ListProduct), produit.CategorieId);
        }
    }
}
