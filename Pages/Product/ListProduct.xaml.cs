﻿using ClientGestionVin.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using ClientGestionVin;
using Syncfusion.UI.Xaml.Grid;
using System.Linq;
using Windows.UI.Xaml;
using ClientGestionVin.Models;
using Windows.UI.Xaml.Navigation;
using ClientGestionVin.Repository;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.UI.WindowManagement;
using ClientGestionVin.Client.Models;

namespace ClientGestionVin.Pages.Product
{
    public sealed partial class ListProduct : Page
    {
        public int CategoryId;
        private ProductRepository productRepository;
        private DomainRepository domainRepository;
        private CategoryRepository categoryRepository;
        private CustomerOrderRepository customerOrderRepository;
        private DomainOrderRepository domainOrderRepository;
        private Dictionary<int, string> domainsDictionary;
        private Dictionary<int, string> categoriesDictionary;
        private Dictionary<int, int> likedOrder;
        private List<CustomerOrder> customersOrders;
        private List<DomainOrder> domainsOrders;

        public int CurrentCategory { get; private set; }

        public ListProduct()
        {
            domainRepository = new DomainRepository();
            productRepository = new ProductRepository();
            categoryRepository = new CategoryRepository();
            customerOrderRepository = new CustomerOrderRepository();
            domainOrderRepository = new DomainOrderRepository();

            fetchAsyncObjects();

            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            int CategoryId = int.Parse(string.Format("{0}", parameters));
            CurrentCategory = CategoryId;

            if (90 == CategoryId)
            {
                getList();
                ContentFrame.Navigate(typeof(ProductForm));
            }
            else
            {
                getListById(CategoryId);
                ContentFrame.Navigate(typeof(ProductForm));
            }
        }

        private async void fetchAsyncObjects()
        {
            await fetchDomainsAsync();
            await fetchCategoriesAsync();
            await fetchOrderAsync();
        }

        private async Task fetchOrderAsync()
        {
            customersOrders = await customerOrderRepository.GetCustomerOrders();
            domainsOrders = await domainOrderRepository.GetDomainOrders();

            countOrderLiked();
        }

        private async Task fetchCategoriesAsync()
        {
            List<Models.Category> categories = await categoryRepository.GetCategories();
            categoriesDictionary = new Dictionary<int, string>();
            
            foreach (var item in categories)
            {
                categoriesDictionary[(int)item.Id] = item.Designation;
            }
        }
      
        private async Task fetchDomainsAsync()
        {
            List<Models.Domain> domains = await domainRepository.GetDomains();
            domainsDictionary = new Dictionary<int, string>();

            foreach (var item in domains)
            {
                domainsDictionary[(int)item.Id] = item.Designation;
            }
        }

        private async void getList()
        {
            ApiGestionVin api = new ApiGestionVin("http://localhost:52452/api/");
            List<Models.Category> categories = await categoryRepository.GetCategories();
            List<Models.Domain> domains = await domainRepository.GetDomains();
            List<Models.Product> products = await productRepository.GetProducts();
            foreach (Models.Product product in products)
            {
                product.Famille = categoriesDictionary[product.CategorieId];
                product.Fournisseur = domainsDictionary[product.DomainId];
            }
            lstProducts.ItemsSource = products;
        }

        private async void getListById(int id)
        {
            List<Models.Category> categories = await categoryRepository.GetCategories();
            List<Models.Product> products = await productRepository.GetProducts();
            List<Models.Product> lstProductsByCategory = new List<Models.Product>();
            List<Models.Domain> domains = await domainRepository.GetDomains();

            foreach (Models.Product product in products)
            {
                product.Famille = categoriesDictionary[product.CategorieId];
                product.Fournisseur = domainsDictionary[product.DomainId];
                if (product.CategorieId == id)
                {
                    lstProductsByCategory.Add(product);
                }
            }

            lstProducts.ItemsSource = lstProductsByCategory;
        }

        private void Edit_Product(object sender, object args)
        {
            var button = sender as Button;

            ContentFrame.Navigate(typeof(ProductForm), button.Tag);
        }

        private void countOrderLiked()
        {
            likedOrder = new Dictionary<int, int>();

            foreach (var item in customersOrders)
            {
                likedOrder[item.ProductId] = 1;
            }
            foreach (var item in customersOrders)
            {
                likedOrder[item.ProductId] = 1;
            }
        }

        private async void Confirm_Del_Product(object sender, object args)
        {
            var button = sender as Button;
            var productId = (int)button.Tag;

            bool delImpossible;
            try
            {
                var stock = likedOrder[productId];
                delImpossible = true;
            }
            catch (Exception)
            {
                delImpossible = false;
            }

            if (delImpossible)
            {
                ContentDialog delIsPossible = new ContentDialog
                {
                    Title = "Confirmation",
                    Content = "Suppression impossible, des commandes sont enregistrés avec ce produit",
                    CloseButtonText = "ok"
                };
                _ = await delIsPossible.ShowAsync();

                Frame.Navigate(typeof(ListProduct), CurrentCategory);
                return;
            }



            ContentDialog delConfirmDialog = new ContentDialog
            {
                Title = "Confirmation",
                Content = "Voulez-vous vraiment supprimer ce produit?",
                PrimaryButtonText = "Oui",
                CloseButtonText = "Non"
            };

            ContentDialogResult result = await delConfirmDialog.ShowAsync();

            switch (result)
            {
                case ContentDialogResult.None:
                    break;
                case ContentDialogResult.Primary:
                    _ = await productRepository.DeleteProduct((int)button.Tag);
                    break;
            }
         
            Frame.Navigate(typeof(ListProduct), CurrentCategory);
        }

        private void Button_Ajout(object sender, object args)
        {
            ContentFrame.Navigate(typeof(ProductForm));
        }
    }
}
