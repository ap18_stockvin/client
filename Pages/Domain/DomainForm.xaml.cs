﻿using ClientGestionVin.Pages.Product;
using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace ClientGestionVin.Pages.Domain
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class DomainForm : Page
    {
        private DomainRepository domainRepository;
        private ObservableCollection<String> suggestions;
        public List<Models.Domain> Domains { get; private set; }
        public int Editing { get; private set; }

        public DomainForm()
        {
            domainRepository = new DomainRepository();
            suggestions = new ObservableCollection<string>();

            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            if (null != parameters)
            {
                int DomainId = int.Parse(string.Format("{0}", parameters));
                Editing = DomainId;
                if (0 != Editing)
                {
                    FetchDomain();
                }
            }
        }
        private async void FetchDomain()
        {
            Models.Domain domain = await domainRepository.GetDomain(Editing);
            DomainDesignation.Text = domain.Designation;
        }
        private async void SaveDomainAsync(object sender, object args)
        {
            Models.Domain domain = new Models.Domain
            {
                Designation = DomainDesignation.Text,
            };

            if (0 != Editing)
            {
                domain.Id = Editing;
                _ = await domainRepository.UpdateDomain(domain);
            }
            else
            {
                _ = await domainRepository.CreateDomain(domain);
            }

            var grid = VisualTreeHelper.GetParent(this.Frame);
            var page = VisualTreeHelper.GetParent(grid);
            var contentPresenter = VisualTreeHelper.GetParent(page);
            var frame = VisualTreeHelper.GetParent(contentPresenter) as Frame;

            frame.Navigate(typeof(ListDomain), 90);
        }
    }
}
