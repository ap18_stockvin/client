﻿using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace ClientGestionVin.Pages.Domain
{
    public sealed partial class ListDomain : Page
    {
        private DomainRepository domainRepository;
        private ProductRepository productRepository;

        private Dictionary<int, int> stockDomain;

        private List<Models.Domain> domains;
        private List<Models.Product> products;

        private Frame frameList;

        public ListDomain()
        {
            domainRepository = new DomainRepository();
            productRepository = new ProductRepository();

            this.InitializeComponent();

            frameList = this.Frame;
            ListContentFrame.Navigate(typeof(DomainForm));

            getList();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;

        }

        private async void getList()
        {
            domains = await domainRepository.GetDomains();
            products = await productRepository.GetProducts();

            countStock();

            foreach (var item in domains)
            {
                try
                {
                    item.Quantite = stockDomain[(int)item.Id];
                }
                catch (Exception)
                { }
            }

            lstDomains.ItemsSource = domains;
        }

        private void countStock()
        {
            stockDomain = new Dictionary<int, int>();

            foreach (var item in products)
            {
                try
                {
                    stockDomain[item.DomainId] += item.Quantite;
                }
                catch (Exception)
                {
                    stockDomain[item.DomainId] = item.Quantite;
                }
            }
        }

        private void Edit_Domain(object sender, object args)
        {
            var button = sender as Button;

            ListContentFrame.Navigate(typeof(DomainForm), button.Tag);
        }

        private async void Confirm_Del_Domain(object sender, object args)
        {
            var button = sender as Button;
            int domainId = (int)button.Tag;

            bool delImpossible;
            try
            {
                var stock = stockDomain[domainId];
                delImpossible = true;
            }
            catch (Exception)
            {
                delImpossible = false;
            }

            if (delImpossible)
            {
                ContentDialog delIsPossible = new ContentDialog
                {
                    Title = "Confirmation",
                    Content = "Suppression impossible, des produits dont enregistrés pour ce Domaine",
                    CloseButtonText = "ok"
                };
                _ = await delIsPossible.ShowAsync();

                Frame.Navigate(typeof(ListDomain));
                return;
            }

            ContentDialog delConfirmDialog = new ContentDialog
            {
                Title = "Confirmation",
                Content = "Voulez-vous vraiment supprimer le fournisseur?",
                PrimaryButtonText = "Oui",
                CloseButtonText = "Non"
            };

            ContentDialogResult result = await delConfirmDialog.ShowAsync();

            switch (result)
            {
                case ContentDialogResult.None:
                    break;
                case ContentDialogResult.Primary:
                    _ = await domainRepository.DeleteDomain((int)button.Tag);
                    break;
            }

            Frame.Navigate(typeof(ListDomain));
        }

        private void Button_Ajout(object sender, object args)
        {
            ListContentFrame.Navigate(typeof(DomainForm));
        }
    }
}

