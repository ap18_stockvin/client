﻿using ClientGestionVin.Pages.Product;
using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace ClientGestionVin.Pages.Category
{
    public sealed partial class ListCategory : Page
    {
        private CategoryRepository categoryRepository;
        private ProductRepository productRepository;

        private Dictionary<int, int> stockProduit;

        private List<Models.Category> categories;
        private List<Models.Product> products;

        public ListCategory()
        {
            categoryRepository = new CategoryRepository();
            productRepository = new ProductRepository();

            fetchAsyncObjects();

            this.InitializeComponent();
            ListContentFrame.Navigate(typeof(CategoryForm));

            getList();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
        }

        private async void fetchAsyncObjects()
        {
            categories = await categoryRepository.GetCategories();
            products = await productRepository.GetProducts();
        }

        private void countStock()
        {
            stockProduit = new Dictionary<int, int>();

            foreach (var item in products)
            {
                try
                {
                    stockProduit[item.CategorieId] += item.Quantite;
                }
                catch (Exception)
                {
                    stockProduit[item.CategorieId] = item.Quantite;
                }
            }
        }

        private async void getList()
        {
            categories = await categoryRepository.GetCategories();
            products = await productRepository.GetProducts();

            countStock();

            foreach (var item in categories)
            {
                try
                {
                    item.Quantite = stockProduit[(int)item.Id];
                }
                catch (Exception)
                { }
            }

            lstCategories.ItemsSource = categories;
        }

        private void Edit_Category(object sender, object args)
        {
            var button = sender as Button;

            ListContentFrame.Navigate(typeof(CategoryForm), button.Tag);
        }

        private async void Confirm_Del_Category(object sender, object args)
        {
            var button = sender as Button;
            int categoryId = (int)button.Tag;
            //todo: suppr impossible
            bool delImpossible;
            try
            {
                var stock = stockProduit[categoryId];
                delImpossible = true;
            }
            catch (Exception)
            {
                delImpossible = false;
            }

            if (delImpossible)
            {
                ContentDialog delIsPossible = new ContentDialog
                {
                    Title = "Confirmation",
                    Content = "Suppression impossible, des produits dont enregistrés pour cette catégorie",
                    CloseButtonText = "ok"
                };
                _ = await delIsPossible.ShowAsync();

                Frame.Navigate(typeof(ListCategory));
                return;
            }

            ContentDialog delConfirmDialog = new ContentDialog
            {
                Title = "Confirmation",
                Content = "Voulez-vous vraiment supprimer la famille?",
                PrimaryButtonText = "Oui",
                CloseButtonText = "Non"
            };

            ContentDialogResult result = await delConfirmDialog.ShowAsync();

            switch (result)
            {
                case ContentDialogResult.None:
                    break;
                case ContentDialogResult.Primary:
                    _ = await categoryRepository.DeleteCategory(categoryId);
                    break;
            }
            Frame.Navigate(typeof(ListCategory));
        }

        private void Button_Ajout(object sender, object args)
        {
            ListContentFrame.Navigate(typeof(CategoryForm));
        }

        private void Stock(object sender, object args)
        {
            var button = sender as Button;
                      
            Frame.Navigate(typeof(ListProduct), button.Tag);
        }
    }
}