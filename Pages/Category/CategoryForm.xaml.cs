﻿using ClientGestionVin.Pages.Product;
using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ClientGestionVin.Client.Models;


namespace ClientGestionVin.Pages.Category
{

    public sealed partial class CategoryForm : Page
    {
        private CategoryRepository categoryRepository;
        private ObservableCollection<String> suggestions;
        public List<Models.Category> Categories { get; private set; }
        public int Editing { get; private set; }

        public CategoryForm()
        {
            categoryRepository = new CategoryRepository();

            suggestions = new ObservableCollection<string>();



            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            if (null != parameters)
            {
                int CategoryId = int.Parse(string.Format("{0}", parameters));
                Editing = CategoryId;
                if (0 != Editing)
                {
                    FetchCategory();
                }
            }
        }
        private async void FetchCategory()
        {
            Models.Category category = await categoryRepository.GetCategory(Editing);
            CategoryDesignation.Text = category.Designation;
        }
        private async void SaveCategoryAsync(object sender, object args)
        {

            Models.Category category = new Models.Category
            {
                Designation = CategoryDesignation.Text,
            };

            if (0 != Editing)
            {
                category.Id = Editing;
                _ = await categoryRepository.UpdateCategory(category);
            }
            else
            {
                _ = await categoryRepository.CreateCategory(category);
            }

            var grid = VisualTreeHelper.GetParent(this.Frame);
            var page = VisualTreeHelper.GetParent(grid);
            var contentPresenter = VisualTreeHelper.GetParent(page);
            var frame = VisualTreeHelper.GetParent(contentPresenter) as Frame;

            frame.Navigate(typeof(ListCategory));
        }
    }
}

