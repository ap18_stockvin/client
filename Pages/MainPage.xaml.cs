﻿using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using ClientGestionVin.Pages;
using ClientGestionVin.Pages.Category;
using ClientGestionVin.Pages.Customer;
using ClientGestionVin.Pages.Domain;
using ClientGestionVin.Pages.Order;
using ClientGestionVin.Pages.Product;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientGestionVin
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            ContentFrame.Navigate(typeof(ListProduct), 90);
        }
        private void NavView_Loaded(object sender, RoutedEventArgs e)
        {
            // set the initial SelectedItem 
            foreach (NavigationViewItemBase item in NavView.MenuItems)
            {
                if (item is NavigationViewItem && item.Tag.ToString() == "inventaire")
                {
                    NavView.SelectedItem = item;
                    break;
                }
            }
        }
        private void NavView_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            if (args.IsSettingsInvoked)
            {
                ContentFrame.Navigate(typeof(DashBord));
            }
            else
            {
                // find NavigationViewItem with Content that equals InvokedItem
                var item = sender.MenuItems.OfType<NavigationViewItem>().First(x => (string)x.Content == (string)args.InvokedItem);
                NavView_Navigate(item as NavigationViewItem);
            }
        }
        private void NavView_Navigate(NavigationViewItem item)
        {
            switch (item.Tag)
            {
                case "customerOrder":
                    ContentFrame.Navigate(typeof(ListCustomerOrder), 0);

                    break;

                case "domainOrder":
                    ContentFrame.Navigate(typeof(ListDomainOrder), 0);
                    break;

                case "customerOrderHistory":
                    ContentFrame.Navigate(typeof(ListCustomerOrder), 1);
                    break;

                case "domainOrderHistory":
                    ContentFrame.Navigate(typeof(ListDomainOrder), 1);
                    break;

                case "inventaire":
                    ContentFrame.Navigate(typeof(DashBord));
                    break;

                case "Customer":
                    ContentFrame.Navigate(typeof(ListCustomer));
                    break;

                case "Domain":
                    ContentFrame.Navigate(typeof(ListDomain));
                    break;

                case "Category":
                    ContentFrame.Navigate(typeof(ListCategory));
                    break;
            }
        }
    }
}