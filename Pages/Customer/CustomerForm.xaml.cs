﻿using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using ClientGestionVin.Pages.Product;
using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace ClientGestionVin.Pages.Customer
{
    public sealed partial class CustomerForm : Page
    {
        private CustomerRepository customerRepository;

        public int Editing { get; private set; }

        public CustomerForm()
        {
            customerRepository = new CustomerRepository();
         
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            if (null != parameters)
            {
                int ProductId = int.Parse(string.Format("{0}", parameters));
                Editing = ProductId;
                if (0 != Editing)
                {
                    FetchCustomer();
                }
            }
        }

        private async void FetchCustomer()
        {
            Client.Models.Customer customer = await customerRepository.GetCustomer(Editing);
            Prenom.Text = customer.Prenom;
            Nom.Text = customer.Nom;
            Societe.Text = (null == customer.Societe)?"": customer.Societe;
        }

        private async void SaveCustomerAsync(object sender, object args)
        {
            Client.Models.Customer customer = new Client.Models.Customer 
            {
                Prenom = Prenom.Text,
                Nom = Nom.Text,
                Societe = Societe.Text
            };

             if (0 != Editing)
            {
                customer.Id = Editing;
                _ = await customerRepository.UpdateCustomer(customer);
            }
            else
            {
                _ = await customerRepository.CreateCustomer(customer);
            }

            var grid = VisualTreeHelper.GetParent(this.Frame);
            var page = VisualTreeHelper.GetParent(grid);
            var contentPresenter = VisualTreeHelper.GetParent(page);
            var frame = VisualTreeHelper.GetParent(contentPresenter) as Frame;

            frame.Navigate(typeof(ListCustomer));
        }
    }
}
