﻿using ClientGestionVin.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using ClientGestionVin;
using Syncfusion.UI.Xaml.Grid;
using System.Linq;
using Windows.UI.Xaml;
using ClientGestionVin.Models;
using Windows.UI.Xaml.Navigation;
using ClientGestionVin.Repository;
using ClientGestionVin.Client.Models;

namespace ClientGestionVin.Pages.Customer
{
    public sealed partial class ListCustomer : Page
    {
        private CustomerRepository customerRepository;
        private CustomerOrderRepository customerOrderRepository;
       
        private Dictionary<int, int> likedOrder;
        private List<Client.Models.Customer> customers;
        private List<CustomerOrder> customerOrders;

        public ListCustomer()
        {
            customerRepository = new CustomerRepository();
            customerOrderRepository = new CustomerOrderRepository();

            this.InitializeComponent();

            getList();

            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            ContentFrame.Navigate(typeof(CustomerForm));

        }

        private void countOrderLiked()
        {
            likedOrder = new Dictionary<int, int>();

            foreach (var item in customerOrders)
            {
                likedOrder[item.CustomerId] = 1;
            }
        }

        private async void getList()
        {
            customers = await customerRepository.GetCustomers();
            customerOrders = await customerOrderRepository.GetCustomerOrders();

            countOrderLiked();
            LstCustomers.ItemsSource = customers;
        }

        private void Edit_Customer(object sender, object args)
        {
            var button = sender as Button;

            ContentFrame.Navigate(typeof(CustomerForm), button.Tag);
        }

        private async void Confirm_Del_Customer(object sender, object args)
        {
            var button = sender as Button;
            int customerId = (int)button.Tag;

            bool delImpossible;

            try
            {
                var stock = likedOrder[customerId];
                delImpossible = true;
            }
            catch (Exception)
            {
                delImpossible = false;
            }

            if (delImpossible)
            {
                ContentDialog delIsPossible = new ContentDialog
                {
                    Title = "Confirmation",
                    Content = "Suppression impossible, des commandes sont liés à ce client",
                    CloseButtonText = "ok"
                };
                _ = await delIsPossible.ShowAsync();

                Frame.Navigate(typeof(ListCustomer));
                return;
            }

            ContentDialog delConfirmDialog = new ContentDialog
            {
                Title = "Confirmation",
                Content = "Voulez-vous vraiment supprimer le fournisseur?",
                PrimaryButtonText = "Oui",
                CloseButtonText = "Non"
            };

            ContentDialogResult result = await delConfirmDialog.ShowAsync();

            switch (result)
            {
                case ContentDialogResult.None:
                    break;
                case ContentDialogResult.Primary:
                    _ = await customerRepository.DeleteCustomer((int)button.Tag);
                    break;
            }
            Frame.Navigate(typeof(ListCustomer));
        }


        private void Button_Ajout(object sender, object args)
        {
            ContentFrame.Navigate(typeof(CustomerForm));
        }
    }
}
