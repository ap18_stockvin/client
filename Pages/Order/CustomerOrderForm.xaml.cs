﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using ClientGestionVin.Pages.Product;
using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace ClientGestionVin.Pages.Order
{
    public sealed partial class CustomerOrderForm : Page
    {
        private ProductRepository productRepository;
        private CustomerRepository customerRepository;
        private CustomerOrderRepository customerOrderRepository;
        private DomainOrderRepository domainOrderRepository;
        private ObservableCollection<string> suggestions;
        private Dictionary<int, string> customersDictionary;
        private Dictionary<int, string> productsDictionary;

        public CustomerOrderForm()
        {
            productRepository = new ProductRepository();
            customerRepository = new CustomerRepository();
            customerOrderRepository = new CustomerOrderRepository();
            domainOrderRepository = new DomainOrderRepository();

            suggestions = new ObservableCollection<string>();

            fetchCustomersSuggestion();
            fetchProductsSuggestion();

         
            this.InitializeComponent();
        }

        private async void fetchCustomersSuggestion()
        {
            List<Client.Models.Customer> customers = await customerRepository.GetCustomers();
            customersDictionary = new Dictionary<int, string>();


            foreach (var item in customers)
            {
                if (null != item.Prenom && null != item.Nom)
                {
                    customersDictionary[(int)item.Id] = item.Prenom + " " + item.Nom;
                    if (null != item.Societe)
                    {
                        customersDictionary[(int)item.Id] += " - ";
                    }
                }
                if (null != item.Societe)
                {
                    customersDictionary[(int)item.Id] += item.Societe;
                }
            }
        }
        
        private async void fetchProductsSuggestion()
        {
            List<Models.Product> products = await productRepository.GetProducts();
            productsDictionary = new Dictionary<int, string>();

            foreach (var item in products)
            {
                productsDictionary[(int)item.Id] = item.Designation;
            }
        }

        public void Customer_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                suggestions.Clear();

                foreach (var item in customersDictionary)
                {
                    if (Regex.IsMatch(item.Value, "(?i)" + sender.Text))
                    {
                        suggestions.Add(item.Value);
                    }
                }
                sender.ItemsSource = suggestions;
            }
        }

        public void Customer_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                Customer.Text = args.ChosenSuggestion.ToString();
            }
            else
            {
                Customer.Text = sender.Text;
            }
        }

        public void Customer_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            Customer.Text = (string)args.SelectedItem;
        }

        public void Product_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                suggestions.Clear();

                foreach (var item in productsDictionary)
                {
                    if (Regex.IsMatch(item.Value, "(?i)" + sender.Text))
                    {
                        suggestions.Add(item.Value);
                    }
                }
                sender.ItemsSource = suggestions;
            }
        }

        public void Product_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                Product.Text = args.ChosenSuggestion.ToString();
            }
            else
            {
                Product.Text = sender.Text;
            }
        }

        public void Product_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            Product.Text = (string)args.SelectedItem;
        }

        private async void SaveCustomerOrderAsync(object sender, object args)
        {
            int CustomerId = 0;
            int ProductId = 0;
            Models.Product product = null;

            foreach (var customer in customersDictionary)
            {
                if (customer.Value == (string)Customer.Text)
                {
                    CustomerId = customer.Key;
                    continue;
                }
            }

            foreach (var productList in productsDictionary)
            {
                if (productList.Value == (string)Product.Text)
                {
                    ProductId = productList.Key;
                    product = await productRepository.GetProduct(ProductId);
                    continue;
                }
            }

            //nouvelle commande si stock insufisant
            int ProductQuantity = int.Parse(OrderQuantity.Text);

            var target = typeof(ListCustomerOrder);

            if ((product.Quantite - ProductQuantity) < product.Seuil)
            {
                int newQuantity = product.Seuil + ProductQuantity - product.Quantite;
                DomainOrder order = new DomainOrder
                {
                    ProductId = ProductId,
                    DomainId = product.DomainId,
                    Date = DateTime.UtcNow,
                    Quantite = newQuantity
                };

                await domainOrderRepository.CreateDomainOrder(order);
                target = typeof(ListCustomerOrder);
            }

            if (0 != CustomerId && 0 != ProductId)
            {

                CustomerOrder order = new CustomerOrder
                {
                    CustomerId = CustomerId,
                    ProductId = ProductId,
                    Quantite = ProductQuantity,
                    Date = DateTime.UtcNow,
                    IsRegister = false
                };
                await customerOrderRepository.CreateCustomerOrder(order);
            }

            var grid = VisualTreeHelper.GetParent(Frame);
            var page = VisualTreeHelper.GetParent(grid);
            var contentPresenter = VisualTreeHelper.GetParent(page);
            var frame = VisualTreeHelper.GetParent(contentPresenter) as Frame;

            frame.Navigate(target, 0);
        }
    }
}