﻿using ClientGestionVin.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using ClientGestionVin;
using Syncfusion.UI.Xaml.Grid;
using System.Linq;
using Windows.UI.Xaml;
using ClientGestionVin.Models;
using Windows.UI.Xaml.Navigation;
using ClientGestionVin.Repository;
using System.Threading.Tasks;
using ClientGestionVin.Client.Models;
using Microsoft.Toolkit.Uwp.UI.Controls.TextToolbarSymbols;

namespace ClientGestionVin.Pages.Order
{
    public sealed partial class ListDomainOrder : Page
    {
        private ProductRepository productRepository;

        public bool ProductsLoaded { get; private set; }
        private Dictionary<int, Models.Product> productsDictionary;
        private Dictionary<int, Models.Domain> domainsDictionary;
        private DomainOrderRepository domainOrderRepository;
        private DomainRepository domainRepository;

        
        internal List<DomainOrder> ListOrder { get; private set; }
        public List<Models.Product> ListProduct { get; private set; }
        public int HistoryView { get; private set; }

        public ListDomainOrder()
        {
            domainOrderRepository = new DomainOrderRepository();
            domainRepository = new DomainRepository();
            productRepository = new ProductRepository();

            fetchAsyncObjects();

            this.InitializeComponent();
        }

        private async void fetchAsyncObjects()
        {
            await fetchDomainsAsync();
            await fetchProductsAsync();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            int History = int.Parse(string.Format("{0}", parameters));
            HistoryView = History;

            if (0 == HistoryView)
            {
                ContentFrame.Navigate(typeof(DomainOrderForm));
                getList();
            }
            else
            {
                getHistoryList();
            }
        }
        
        private async void getHistoryList()
        {
            ListOrder = await domainOrderRepository.GetDomainOrders();

            await fetchDomainsAsync();
            await fetchProductsAsync();

            List<DomainOrder> ListOrderToShow = new List<DomainOrder>();

            foreach (var item in ListOrder)
            {
                if (item.IsRegister)
                {
                    item.Fournisseur = domainsDictionary[item.DomainId].Designation;
                    item.DesignationProduit = productsDictionary[item.ProductId].Designation;
                    item.TU = productsDictionary[item.ProductId].Prix;
                    item.Total = item.Quantite * productsDictionary[item.ProductId].Prix;
                    item.Visibility = "Collapsed";
                    ListOrderToShow.Add(item);
                }
            }

            LstDomainsOrders.ItemsSource = ListOrderToShow;
        }

        private async void getList()
        {
            ListOrder = await domainOrderRepository.GetDomainOrders();
            List<DomainOrder> ListOrderToShow = new List<DomainOrder>();

            await fetchDomainsAsync();
            await fetchProductsAsync();

            foreach (var item in ListOrder)
            {
                if (!item.IsRegister)
                {
                    item.Fournisseur = domainsDictionary[item.DomainId].Designation;
                    item.DesignationProduit = productsDictionary[item.ProductId].Designation;
                    item.TU = productsDictionary[item.ProductId].Prix;
                    item.Total = item.Quantite * productsDictionary[item.ProductId].Prix;
                    item.Visibility = "Visible";
                    ListOrderToShow.Add(item);
                }
            }

            LstDomainsOrders.ItemsSource = ListOrderToShow;
        }

        private async Task fetchDomainsAsync()
        {
            List<Models.Domain> domains = await domainRepository.GetDomains();
            domainsDictionary = new Dictionary<int, Models.Domain>();

            foreach (var item in domains)
            {
                  domainsDictionary[(int)item.Id] = item;
            }
        }

        private async Task fetchProductsAsync()
        {
            List<Models.Product> products = await productRepository.GetProducts();
            productsDictionary = new Dictionary<int, Models.Product>();

            foreach (var item in products)
            {
                productsDictionary[(int)item.Id] = item;
            }
        }

        private async Task fetchAll()
        {
            List<Models.Product> products = await productRepository.GetProducts();
            List<Models.Domain> domains = await domainRepository.GetDomains();
            productsDictionary = new Dictionary<int, Models.Product>();
            domainsDictionary = new Dictionary<int, Models.Domain>();

            //todo domainIdDefined?

            foreach (var item in products)
            {
                productsDictionary[(int)item.Id] = item;
            }
            
            foreach (var item in domains)
            {
                domainsDictionary[(int)item.Id] = item;
            }
        }

        private async void Valid_Order(object sender, object args)
        {
            var button = sender as Button;
            int id = (int)button.Tag;

            DomainOrder domainOrder = await domainOrderRepository.GetDomainOrder(id);
            Models.Product product = await productRepository.GetProduct(domainOrder.ProductId);

            product.Quantite += domainOrder.Quantite;
            await productRepository.UpdateProduct(product);

            domainOrder.IsRegister = true;
            await domainOrderRepository.UpdateDomainOrder(domainOrder);

            Frame.Navigate(typeof(ListDomainOrder), 0);
        }

        private async void Confirm_Del_Order(object sender, object args)
        {
            var button = sender as Button;

            ContentDialog delConfirmDialog = new ContentDialog
            {
                Title = "Confirmation",
                Content = "Voulez-vous vraiment supprimer la comamnde?",
                PrimaryButtonText = "Oui",
                CloseButtonText = "Non"
            };

            ContentDialogResult result = await delConfirmDialog.ShowAsync();

            switch (result)
            {
                case ContentDialogResult.None:
                    break;
                case ContentDialogResult.Primary:
                    _ = await domainOrderRepository.DeleteDomainOrder((int)button.Tag);
                    break;
            }

            Frame.Navigate(typeof(ListDomainOrder), 0);
        }
    }
}
