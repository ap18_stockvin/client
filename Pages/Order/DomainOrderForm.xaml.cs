﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using ClientGestionVin.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace ClientGestionVin.Pages.Order
{
    public sealed partial class DomainOrderForm : Page
    {
        private ProductRepository productRepository;
        private DomainRepository domainRepository;
        private DomainOrderRepository domainOrderRepository;

        private ObservableCollection<string> suggestions;
        private Dictionary<int, string> domainsDictionary;
        private Dictionary<int, string> productsDictionary;

        private List<Models.Product> listProducs;

        private string domainChosen;

        public DomainOrderForm()
        {
            domainOrderRepository = new DomainOrderRepository();
            domainRepository = new DomainRepository();
            productRepository = new ProductRepository();

            suggestions = new ObservableCollection<string>();

            fetchDomainsSuggestion();
            fetchProductsSuggestion();
         
            this.InitializeComponent();
        }

        private async void fetchDomainsSuggestion()
        {
            List<Models.Domain> domains = await domainRepository.GetDomains();
            domainsDictionary = new Dictionary<int, string>();


            foreach (var item in domains)
            {
                if (null != item.Designation)
                {
                    domainsDictionary[(int)item.Id] = item.Designation;
                }
            }
        }
        
        private async void fetchProductsSuggestion()
        {
            listProducs = await productRepository.GetProducts();
            productsDictionary = new Dictionary<int, string>();

            foreach (var item in listProducs)
            {
                if (null == domainChosen)
                {
                    productsDictionary[(int)item.Id] = item.Designation;
                }
                else if (domainChosen == item.DomainId.ToString())
                {
                    productsDictionary[(int)item.Id] = item.Designation;
                }
            }
        }

        public void Domain_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                suggestions.Clear();

                foreach (var item in domainsDictionary)
                {
                    if (Regex.IsMatch(item.Value, "(?i)" + sender.Text))
                    {
                        suggestions.Add(item.Value);
                    }
                }
                sender.ItemsSource = suggestions;
            }
        }

        public void Domain_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                Domain.Text = args.ChosenSuggestion.ToString();
                domainChosen = args.ChosenSuggestion.ToString();
            }
            else
            {
                Domain.Text = sender.Text;
            }
        }

        public void Domain_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            Domain.Text = (string)args.SelectedItem;
        }

        public void Product_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                suggestions.Clear();

                //domain choisis
                if (null != domainChosen)
                {
                    foreach (var item in listProducs)
                    {
                        if (domainChosen == domainsDictionary[item.DomainId])
                        {
                            suggestions.Add(productsDictionary[(int)item.Id]);
                        }
                    }
                } else
                {
                    foreach (var item in productsDictionary)
                    {
                        if (Regex.IsMatch(item.Value, "(?i)" + sender.Text))
                        {
                            suggestions.Add(item.Value);
                        }
                    }
                }
                sender.ItemsSource = suggestions;
            }
        }

        public void Product_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                Product.Text = args.ChosenSuggestion.ToString();
            }
            else
            {
                Product.Text = sender.Text;
            }
        }

        public void Product_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            Product.Text = (string)args.SelectedItem;
        }

        private async void SaveDomainOrderAsync(object sender, object args)
        {
            int DomainId = 0;
            int ProductId = 0;

            foreach (var customer in domainsDictionary)
            {
                if (customer.Value == (string)Domain.Text)
                {
                    DomainId = customer.Key;
                    continue;
                }
            }

            foreach (var product in productsDictionary)
            {
                if (product.Value == (string)Product.Text)
                {
                    ProductId = product.Key;
                    continue;
                }
            }

            if (0 != DomainId && 0 != ProductId)
            {
                DomainOrder order = new DomainOrder
                {
                    DomainId = DomainId,
                    ProductId = ProductId,
                    Quantite = int.Parse(OrderQuantity.Text),
                    Date = DateTime.UtcNow,
                    IsRegister = false
                };
                await domainOrderRepository.CreateDomainOrder(order);
            }

            var grid = VisualTreeHelper.GetParent(Frame);
            var page = VisualTreeHelper.GetParent(grid);
            var contentPresenter = VisualTreeHelper.GetParent(page);
            var frame = VisualTreeHelper.GetParent(contentPresenter) as Frame;

            frame.Navigate(typeof(ListDomainOrder),0);
        }
    }
}
