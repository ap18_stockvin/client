﻿using ClientGestionVin.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using ClientGestionVin;
using Syncfusion.UI.Xaml.Grid;
using System.Linq;
using Windows.UI.Xaml;
using ClientGestionVin.Models;
using Windows.UI.Xaml.Navigation;
using ClientGestionVin.Repository;
using System.Threading.Tasks;
using ClientGestionVin.Client.Models;
using Microsoft.Toolkit.Uwp.UI.Controls.TextToolbarSymbols;

namespace ClientGestionVin.Pages.Order
{
    public sealed partial class ListCustomerOrder : Page
    {
        internal CustomerOrderRepository customerOrderRepository { get; }
        private CustomerRepository customerRepository;
        private ProductRepository productRepository;

        public bool CustomersLoaded { get; private set; }
        private Dictionary<int, Client.Models.Customer> customersDictionary;

        public bool ProductsLoaded { get; private set; }
        private Dictionary<int, Models.Product> productsDictionary;

        internal List<CustomerOrder> ListOrder { get; private set; }
        internal List<Client.Models.Customer> ListCustomer { get; private set; }
        public List<Models.Product> ListProduct { get; private set; }

        private Dictionary<int, bool> enoughtProduct;

        public int HistoryView { get; private set; }

        public ListCustomerOrder()
        {
            customerOrderRepository = new CustomerOrderRepository(); 
            customerRepository = new CustomerRepository();
            productRepository = new ProductRepository();

            enoughtProduct = new Dictionary<int, bool>();

            fetchAsyncObjects();

            this.InitializeComponent();

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var parameters = e.Parameter;
            int History = int.Parse(string.Format("{0}", parameters));
            HistoryView = History;

            if (0 == HistoryView)
            {
                ContentFrame.Navigate(typeof(CustomerOrderForm), 0);
                getList();
            }
            else
            {
                getHistoryList();
            }
        }

        private async void getHistoryList()
        {
            ListOrder = await customerOrderRepository.GetCustomerOrders();

            //Attente chargement des elements
            if (!(CustomersLoaded && ProductsLoaded))
            {
                await fetchCustomersAsync();
                await fetchProductsAsync();
            }

            List<CustomerOrder> ListOrderToShow = new List<CustomerOrder>();

            foreach (var item in ListOrder)
            {
                if (item.IsRegister)
                {
                    item.Prenom = customersDictionary[item.CustomerId].Prenom;
                    item.Nom = customersDictionary[item.CustomerId].Nom;
                    item.Societe = customersDictionary[item.CustomerId].Societe;
                    item.DesignationProduit = productsDictionary[item.ProductId].Designation;
                    item.TU = productsDictionary[item.ProductId].Prix;
                    item.Total = item.Quantite * productsDictionary[item.ProductId].Prix;
                    item.Visibility = "Collapsed";
                    ListOrderToShow.Add(item);
                }
            }

            LstCustomersOrders.ItemsSource = ListOrderToShow;
        }

        private void checkStockForOrder(CustomerOrder item)
        {
            if (item.Quantite >= productsDictionary[item.ProductId].Quantite)
            {
                enoughtProduct[(int)item.Id] = false;
            }
            else
            {
                enoughtProduct[(int)item.Id] = true;
            }
        }

        private async void fetchAsyncObjects()
        {
            await fetchCustomersAsync();
            await fetchProductsAsync();
        }

        private async void getList()
        {
            ListOrder = await customerOrderRepository.GetCustomerOrders();

            //Attente chargement des elements
            if (!(CustomersLoaded && ProductsLoaded))
            {
                await fetchCustomersAsync();
                await fetchProductsAsync();
            }

            List<CustomerOrder> ListOrderToShow = new List<CustomerOrder>();

            foreach (var item in ListOrder)
            {
                if (!item.IsRegister)
                {
                    item.Prenom = customersDictionary[item.CustomerId].Prenom;
                    item.Nom = customersDictionary[item.CustomerId].Nom;
                    item.Societe = customersDictionary[item.CustomerId].Societe;
                    item.DesignationProduit = productsDictionary[item.ProductId].Designation;
                    item.TU = productsDictionary[item.ProductId].Prix;
                    item.Total = item.Quantite * productsDictionary[item.ProductId].Prix;
                    item.Visibility = "Visible";
                    ListOrderToShow.Add(item);
                    checkStockForOrder(item);
                }
            }

            LstCustomersOrders.ItemsSource = ListOrderToShow;
        }

        private async Task fetchCustomersAsync()
        {
            List<Client.Models.Customer> customers = await customerRepository.GetCustomers();
            CustomersLoaded = true;
            customersDictionary = new Dictionary<int, Client.Models.Customer>();

            foreach (var item in customers)
            {
                customersDictionary[(int)item.Id] = item;
            }
        }

        private async Task fetchProductsAsync()
        {
            List<Models.Product> products = await productRepository.GetProducts();
            ProductsLoaded = true;
            productsDictionary = new Dictionary<int, Models.Product>();

            foreach (var item in products)
            {
                productsDictionary[(int)item.Id] = item;
            }
        }

        private async void Valid_Order(object sender, object args)
        {
            var button = sender as Button;
            int orderId = (int)button.Tag;

            if (!enoughtProduct[orderId])
            {
                ContentDialog validNotPossibleDialog = new ContentDialog
                {
                    Title = "Validation impossible",
                    Content = "Stock insuffisant pour honorer la commande, veuillez vous approvisionner",
                    CloseButtonText = "Ok"
                };
                ContentDialogResult result = await validNotPossibleDialog.ShowAsync();

                Frame.Navigate(typeof(ListCustomerOrder), 0);
                return;
            }

            CustomerOrder co = await customerOrderRepository.GetCustomerOrder(orderId);
            Models.Product product = productsDictionary[co.ProductId];

            product.Quantite -= co.Quantite;
            await productRepository.UpdateProduct(product);

            co.IsRegister = true;
            await customerOrderRepository.UpdateCustomerOrder(co);

            Frame.Navigate(typeof(ListCustomerOrder), 0);
        }

        private async void Confirm_Del_Order(object sender, object args)
        {
            var button = sender as Button;
            int orderId = (int)button.Tag;
            
            ContentDialog delConfirmDialog = new ContentDialog
            {
                Title = "Confirmation",
                Content = "Voulez-vous vraiment supprimer la commande?",
                PrimaryButtonText = "Oui",
                CloseButtonText = "Non"
            };

            ContentDialogResult result = await delConfirmDialog.ShowAsync();
            switch (result)
            {
                case ContentDialogResult.None:
                    break;
                case ContentDialogResult.Primary:
                    _ = await customerOrderRepository.DeleteCustomerOrder(orderId);
                    break;
            }
            
            Frame.Navigate(typeof(ListCustomerOrder), 0);
        }
    }
}
