﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using Newtonsoft.Json;
using Syncfusion.XlsIO.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Repository
{
    class DomainOrderRepository
    {
        private ApiGestionVin api;
        private HttpClient httpClient;
        private string baseUrl;

        public DomainOrderRepository()
        {
            api = new ApiGestionVin();
            baseUrl = api.baseUrl;
            httpClient = api.httpClient;
        }

        public async Task<List<DomainOrder>> GetDomainOrders()
        {
            HttpResponseMessage response = null;
            try
            {
                response = await api.httpClient.GetAsync(baseUrl + "DomainOrders");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<List<DomainOrder>>(result);
            }

            return new List<DomainOrder>();
        }

        public async Task<DomainOrder> GetDomainOrder(int id)
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "DomainOrders/" + id);

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<DomainOrder>(result);
            }

            return new DomainOrder();
        }



        public async Task<bool> DeleteDomainOrder(int id)
        {
            HttpResponseMessage response = await httpClient.DeleteAsync(baseUrl + "DomainOrders/" + id);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateDomainOrder(DomainOrder order)
        {
            if (null == order.Id)
            {
                order.Id = 0;
            }

            string productJson = JsonConvert.SerializeObject(order);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.PostAsync(baseUrl + "DomainOrders", content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateDomainOrder(DomainOrder order)
        {
            string productJson = JsonConvert.SerializeObject(order);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            string url = baseUrl + "DomainOrders/" + order.Id;
            HttpResponseMessage response = await httpClient.PutAsync(baseUrl + "DomainOrders/" + order.Id, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }
}
