﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using Newtonsoft.Json;
using Syncfusion.XlsIO.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Repository
{
    class CustomerRepository
    {
        private ApiGestionVin api;
        private HttpClient httpClient;
        private string baseUrl;

        public CustomerRepository()
        {
            api = new ApiGestionVin();
            baseUrl = api.baseUrl;
            httpClient = api.httpClient;
        }

        public async Task<List<Customer>> GetCustomers()
        {
            HttpResponseMessage response = null;
            try
            {
                response = await httpClient.GetAsync(baseUrl + "Customers");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<List<Customer>>(result);
            }

            return new List<Customer>();
        }

        public async Task<Customer> GetCustomer(int id)
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "Customers/" + id);

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Customer>(result);
            }

            return new Customer();
        }



        public async Task<bool> DeleteCustomer(int id)
        {
            HttpResponseMessage response = await httpClient.DeleteAsync(baseUrl + "Customers/" + id);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateCustomer(Customer customer)
        {
            if (null == customer.Id)
            {
                customer.Id = 0;
            }

            string productJson = JsonConvert.SerializeObject(customer);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.PostAsync(baseUrl + "Customers", content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateCustomer(Customer customer)
        {
            string productJson = JsonConvert.SerializeObject(customer);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            string url = baseUrl + "Customers/" + customer.Id;
            HttpResponseMessage response = await httpClient.PutAsync(baseUrl + "Customers/" + customer.Id, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }
}
