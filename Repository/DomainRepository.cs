﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using Newtonsoft.Json;
using Syncfusion.XlsIO.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Repository
{
    class DomainRepository
    {
        private ApiGestionVin api;
        private HttpClient httpClient;
        private string baseUrl;

        public DomainRepository()
        {
            api = new ApiGestionVin();
            baseUrl = api.baseUrl;
            httpClient = api.httpClient;
        }


        public async Task<List<Domain>> GetDomains()
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "domains");

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<List<Domain>>(result);
            }

            return new List<Domain>();
        }
        public async Task<Domain> GetDomain(int id)
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "domains/" + id);

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Domain>(result);
            }

            return new Domain();
        }
        public async Task<bool> DeleteDomain(int id)
        {
            HttpResponseMessage response = await httpClient.DeleteAsync(baseUrl + "Domains/" + id);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
        public async Task<bool> CreateDomain(Domain domain)
        {
            domain.Id = 0;
            string domainJson = JsonConvert.SerializeObject(domain);

            StringContent content = new StringContent(domainJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.PostAsync(baseUrl + "domains", content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
        public async Task<bool> UpdateDomain(Domain domain)
        {
            string domainJson = JsonConvert.SerializeObject(domain);

            StringContent content = new StringContent(domainJson, Encoding.UTF8, "application/json");

            string url = baseUrl + "domains/" + domain.Id;
            HttpResponseMessage response = await httpClient.PutAsync(baseUrl + "domains/" + domain.Id, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }
}
