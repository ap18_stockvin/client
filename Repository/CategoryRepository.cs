﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using Newtonsoft.Json;
using Syncfusion.XlsIO.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Repository
{
    class CategoryRepository
    {
        private ApiGestionVin api;
        private HttpClient httpClient;
        private string baseUrl;

        public CategoryRepository()
        {
            api = new ApiGestionVin();
            baseUrl = api.baseUrl;
            httpClient = api.httpClient;
        }



        public async Task<List<Category>> GetCategories()
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "categories");

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<List<Category>>(result);
            }

            return new List<Category>();
        }
        public async Task<Category> GetCategory(int id)
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "categories/" + id);

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Category>(result);
            }

            return new Category();
        }
        public async Task<bool> DeleteCategory(int id)
        {
            HttpResponseMessage response = await httpClient.DeleteAsync(baseUrl + "Categories/" + id);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateCategory(Category category)
        {
            category.Id = 0;
            string categoryJson = JsonConvert.SerializeObject(category);

            StringContent content = new StringContent(categoryJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.PostAsync(baseUrl + "categories", content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
        public async Task<bool> UpdateCategory(Category category)
        {
            string categoryJson = JsonConvert.SerializeObject(category);

            StringContent content = new StringContent(categoryJson, Encoding.UTF8, "application/json");

            string url = baseUrl + "categories/" + category.Id;
            HttpResponseMessage response = await httpClient.PutAsync(baseUrl + "categories/" + category.Id, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }
}
