﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using Newtonsoft.Json;
using Syncfusion.XlsIO.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Repository
{
    class CustomerOrderRepository
    {
        private ApiGestionVin api;
        private HttpClient httpClient;
        private string baseUrl;

        public CustomerOrderRepository()
        {
            api = new ApiGestionVin();
            baseUrl = api.baseUrl;
            httpClient = api.httpClient;
        }

        public async Task<List<CustomerOrder>> GetCustomerOrders()
        {
            HttpResponseMessage response = null;
            try
            {
                response = await api.httpClient.GetAsync(baseUrl + "CustomerOrders");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<List<CustomerOrder>>(result);
            }

            return new List<CustomerOrder>();
        }

        public async Task<CustomerOrder> GetCustomerOrder(int id)
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "CustomerOrders/" + id);

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<CustomerOrder>(result);
            }

            return new CustomerOrder();
        }



        public async Task<bool> DeleteCustomerOrder(int id)
        {
            HttpResponseMessage response = await httpClient.DeleteAsync(baseUrl + "CustomerOrders/" + id);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateCustomerOrder(CustomerOrder order)
        {
            if (null == order.Id)
            {
                order.Id = 0;
            }

            string productJson = JsonConvert.SerializeObject(order);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.PostAsync(baseUrl + "CustomerOrders", content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateCustomerOrder(CustomerOrder order)
        {
            string productJson = JsonConvert.SerializeObject(order);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            string url = baseUrl + "CustomerOrders/" + order.Id;
            HttpResponseMessage response = await httpClient.PutAsync(baseUrl + "CustomerOrders/" + order.Id, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }
}
