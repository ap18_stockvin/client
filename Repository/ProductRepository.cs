﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Helpers;
using ClientGestionVin.Models;
using Newtonsoft.Json;
using Syncfusion.XlsIO.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Repository
{
    class ProductRepository
    {
        private ApiGestionVin api;
        private HttpClient httpClient;
        private string baseUrl;

        public ProductRepository()
        {
            api = new ApiGestionVin();
            baseUrl = api.baseUrl;
            httpClient = api.httpClient;
        }


        public async Task<List<Product>> GetProducts()
        {
            HttpResponseMessage response = null;
            try
            {
                response = await httpClient.GetAsync(baseUrl + "Products");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<List<Product>>(result);
            }

            return new List<Product>();
        }

        public async Task<Product> GetProduct(int id)
        {
            HttpResponseMessage response = await httpClient.GetAsync(baseUrl + "products/" + id);

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Product>(result);
            }

            return new Product();
        }



        public async Task<bool> DeleteProduct(int id)
        {
            HttpResponseMessage response = await httpClient.DeleteAsync(baseUrl + "products/" + id);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateProduct(Product product)
        {
            if (null == product.Id)
            {
                product.Id = 0;
            }

            string productJson = JsonConvert.SerializeObject(product);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await httpClient.PostAsync(baseUrl + "products", content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateProduct(Product product)
        {
            string productJson = JsonConvert.SerializeObject(product);

            StringContent content = new StringContent(productJson, Encoding.UTF8, "application/json");

            string url = baseUrl + "products/" + product.Id;
            HttpResponseMessage response = await httpClient.PutAsync(baseUrl + "products/" + product.Id, content);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }
}
