﻿using ClientGestionVin.Client.Models;
using ClientGestionVin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ClientGestionVin.Helpers
{
    class ApiGestionVin
    {
        public string baseUrl = "http://localhost:52452/api/";
        public HttpClient httpClient = new HttpClient();

        public bool IsModified { get; set; }
      
        public ApiGestionVin()
        {}

        public ApiGestionVin(string baseUrl)
        {
            this.baseUrl = baseUrl;
        }
    }
}
